# XClipper for Android

![build](https://github.com/KaustubhPatange/XClipper/workflows/Android%20CI/badge.svg)

> An Android XClipper client to communicate with the desktop application to simulate the sharing of clipboard activity.

> This is the F-droid version of **XClipper** which has some limited features (as F-Droid doesn't allow Google related services). But if you want the full version including features like Synchronization to other device, etc. be sure to checkout [Github](https://github.com/KaustubhPatange/XClipper) version.

<img width="120px" src="fastlane/metadata/android/en-US/images/icon.png"/>

## Download

<a target="_blank" href="https://play.google.com/store/apps/details?id=com.kpstv.xclipper"><img width="175px" src="https://camo.githubusercontent.com/f9dc78b44989eb93046dee0cc745b113ae8f9c2c/68747470733a2f2f7777772e62696e672e636f6d2f74683f69643d4f49502e614b56796e464857494546775079454c6b416473775148614353267069643d4170692672733d31"/></a>

## License

- [The Apache License Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt)

```
Copyright 2020 Kaustubh Patange

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
