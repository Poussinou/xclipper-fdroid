import kotlin.reflect.full.memberProperties

@Suppress("unused")
object ModuleDependency {
    const val APP = ":app"
    const val LIBRARY_UTILS = ":library_utils"
    const val PRICING_CARDS = ":pricing"
    const val LINK_PREVIEW = ":link-preview"
    const val COMMON = ":common"

    fun getAllModules(): Set<String> = ModuleDependency::class.memberProperties
        .filter { it.isConst }
        .map { it.getter.call().toString() }
        .toSet()
}